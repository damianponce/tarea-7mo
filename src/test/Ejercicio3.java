package ejercicios;
public class Ejercicio3 {
public static void main(String[] args) {
	System.out.println("Tecla de escape\t\tSignificado");
	System.out.println("===============\t\t===========");
	System.out.println("\\n\t\t\tSignifica nueva linea");
	System.out.println("\\t\t\t\tSignifica un tab de espacio");
	System.out.println("\\\"\t\t\tEs para poner \" (comillas dobles) en texto");
	System.out.println("\\\\\t\t\tSe utiliza para escribir la \\ en texto");
	System.out.println("\\\'\t\t\tSe utiliza para las \' (comilla simple) en texto");
}}