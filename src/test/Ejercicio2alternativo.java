package ejercicios;

import java.util.Scanner;

class Ejercicio2alternativo {

public static void main(String[]args){
	@SuppressWarnings("resource")
	Scanner scanner = new Scanner(System.in);
	
	final Object[][] table = new String[6][];
	table[0] = new String[] { "Nombre", "Apellido", "e-mail", "Materia"};
	table[1] = new String[] { "======", "========", "======", "======="};
	
	int count = 2;
	for (int i = 0; i < 4; i++) {
		System.out.println("\n\nIngrese el nombre del profesor: ");
		String nombre = scanner.nextLine();
		System.out.println("\nIngrese el apellido del profesor: ");
		String apellido = scanner.nextLine();
		System.out.println("\nIngrese el e-mail del profesor: ");
		String mail = scanner.nextLine();
		System.out.println("\nIngrese la materia del profesor: ");
		String materia = scanner.nextLine();
		
		table[count] = new String[] { nombre, apellido, mail, materia};
		
		count++;
	}

	for (final Object[] row : table) {
		System.out.format("%-11s%-15s%-30s%-30s\n", row); }
}

}